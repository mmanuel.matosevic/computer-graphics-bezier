package hr.fer.irg.lab3.vjezba6;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import hr.fer.irg.lab2.vjezba4.iPoligon;
import hr.fer.irg.lab2.vjezba4.iVrh3D;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class Bezijer{
    static {
        GLProfile.initSingleton();
    }


    private static List<RealMatrix> listVrh = new ArrayList<>();
    private static List<iPoligon> listPoligon = new ArrayList<>();
    private static RealMatrix ociste, glediste;
    private static RealMatrix T, T1, T2, T3, T4, T5;
    public static float rotateFactor = 1;
    private static List<RealMatrix> noviVrh = new ArrayList<>();
    private static List<iVrh3D> bersierVrh = new ArrayList<>();
    private static boolean flag = false;
    private static double x_max, x_min, y_max, y_min, z_max, z_min;
    private static List<String> listTocke = new ArrayList<>();
    public static double t = 0;

    static GLProfile glprofile = GLProfile.getDefault();
    static GLCapabilities glcapabilities = new GLCapabilities(glprofile);
    static final GLCanvas glcanvas = new GLCanvas(glcapabilities);
    private static int scale;


    public static void main(String[] args) throws IOException {
        //UCITAVANJE
        Scanner sc = new Scanner(System.in);
        System.out.println("Upisite objekt koji zelite crtati: ");
        String path = "/home/mm51165/Desktop/Objekti/" + sc.next();
        Path p = Path.of(path);
        List<String> listAll = Files.readAllLines(p);

        //OCISTE
        System.out.println("Upisite koordinate ocista");
        double[][] o = new double[4][4];
        for(int i = 0; i<=3; i++){
            for(int j=0; j<=3; j++){
                if(i == 0 && j == 3) o[i][j] = 1;
                else if(i == 0) o[i][j] = sc.nextDouble();
                else o[i][j] = 0;
            }
        }
        ociste = new Array2DRowRealMatrix(o);

        System.out.println("OCISTE je postavljeno na koordinate: x=" + ociste.getEntry(0,0) + " y=" + ociste.getEntry(0,1) + " z=" + ociste.getEntry(0,2) + " " + ociste.getEntry(0,3));


        //GLEDISTE
        /*System.out.println("Upisite koordinate gledista");
        double[][] g = new double[4][4];
        for(int i = 0; i<=3; i++){
            for(int j=0; j<=3; j++){
                if(i == 0 && j == 3) g[i][j] = 1;
                else if(i == 0) g[i][j] = sc.nextDouble();
                else g[i][j] = 0;
            }
        } */
        //glediste = new Array2DRowRealMatrix(g);
        //System.out.println("GLEDISTE je postavljeno na koordinate: x=" + glediste.getEntry(0,0) + " y=" + glediste.getEntry(0,1) + " z=" + glediste.getEntry(0,2) + " " + ociste.getEntry(0,3));


        //UBACIVANJE VRHOVA I POLIGONA U LISTE
        for(String s : listAll){
            if(s.startsWith("v")){
                String[] string = s.split(" ");
                Double x = Double.parseDouble(string[1]);
                Double y = Double.parseDouble(string[2]);
                Double z = Double.parseDouble(string[3]);
                double[][] vrhUMatrici = new double[4][4];
                for(int i=0; i<4; i++){
                    for(int j=0; j<4; j++){
                        if(i == 0 && j == 0) vrhUMatrici[i][j] = x;
                        else if(i == 0 && j == 1) vrhUMatrici[i][j] = y;
                        else if(i == 0 && j == 2) vrhUMatrici[i][j] = z;
                        else if(i == 0 && j == 3) vrhUMatrici[i][j] = 1;
                        else vrhUMatrici[i][j] = 0;
                    }
                }
                RealMatrix vrh = new Array2DRowRealMatrix(vrhUMatrici);
                listVrh.add(vrh);
            }
            else if(s.startsWith("f")){
                String[] string = s.split(" ");
                Integer v1 = Integer.parseInt(string[1]);
                Integer v2 = Integer.parseInt(string[2]);
                Integer v3 = Integer.parseInt(string[3]);
                listPoligon.add(new iPoligon(v1, v2, v3));
            }

        }

        nadiEkstreme(listVrh);
        double[][] g = new double[4][4];
        for(int i=0; i<4; i++){
            for(int j=0; j<4; j++){
                if(i == 0 && j == 0) g[i][j] = (x_max + x_min)/2;
                else if(i == 0 && j == 1) g[i][j] = (y_max + y_min) / 2;
                else if(i == 0 && j == 2) g[i][j] = (z_min + z_max) / 2;
                else if(i == 0 && j == 3) g[i][j] = 1;
                else g[i][j] = 0;
            }
        }
        glediste = new Array2DRowRealMatrix(g);
        System.out.println("GLEDISTE je postavljeno na koordinate: x=" + glediste.getEntry(0,0) + " y=" + glediste.getEntry(0,1) + " z=" + glediste.getEntry(0,2) + " " + ociste.getEntry(0,3));

        System.out.println("Upisite scale za objekt");
        scale = sc.nextInt();

        //sc.close();


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {


                glcanvas.addMouseWheelListener(new MouseWheelListener() {
                    @Override
                    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
                        int rotirano  = mouseWheelEvent.getWheelRotation();
                        rotateFactor = (float) (rotirano > 0? rotateFactor + 0.10 : rotateFactor - 0.10);
                        if (rotateFactor <= 0) rotateFactor = 1;

                        //flagScale = true;
                        glcanvas.display();
                    }
                });

                glcanvas.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if(e.getButton() == 1) {
                            try {
                                String path = "/home/mm51165/Desktop/TockeIRG/tocke";
                                Path p = Path.of(path);
                                List<String> listTocke = Files.readAllLines(p);
                                for(String s : listTocke) {
                                    String[] string = s.split(" ");
                                    Double x = Double.parseDouble(string[0]);
                                    Double y = Double.parseDouble(string[1]);
                                    Double z = Double.parseDouble(string[2]);
                                    bersierVrh.add(new iVrh3D(x,y,z));
                                }
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            System.out.println("BER " + bersierVrh);

                            flag = true;
                            glcanvas.display();
                        }

                    }
                });

                glcanvas.addKeyListener(new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent keyEvent) {

                    }

                    @Override
                    public void keyPressed(KeyEvent keyEvent) {

                        if(flag){
                            if(keyEvent.getKeyChar() == 'o') t += 0.01;
                            if(keyEvent.getKeyChar() == 'i') t -= 0.01;
                        }


                        glcanvas.display();
                    }

                    @Override
                    public void keyReleased(KeyEvent keyEvent) {

                    }
                });

                glcanvas.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        System.out.println("X=" + e.getX() + " Y=" + e.getY());
                    }
                });



                glcanvas.addGLEventListener(new GLEventListener(){
                    @Override
                    public void init(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void dispose(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height) {
                        GL2 gl2 = glautodrawable.getGL().getGL2(); //Returns the GL pipeline object this GLAutoDrawable uses.
                        gl2.glMatrixMode(GL2.GL_PROJECTION); //Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        GLU glu = new GLU();  //Provides access to the OpenGL Utility Library (GLU). This library provides standard methods for setting up view volumes, building mipmaps and performing other common operations.
                        glu.gluOrtho2D(0.0f, width, 0.0f, height);

                        gl2.glMatrixMode(GL2.GL_MODELVIEW);  ////Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        gl2.glViewport(0, 0, width, height); //Entry point to C language function: void glViewport
                    }

                    @Override
                    public void display(GLAutoDrawable glautodrawable) {
                        GL2 gl2	= glautodrawable.getGL().getGL2();  //Returns the GL pipeline object this GLAutoDrawable uses.
                        int width = glautodrawable.getSurfaceWidth();
                        int height = glautodrawable.getSurfaceHeight();

                        gl2.glClear(GL.GL_COLOR_BUFFER_BIT);  //clear buffers to preset values

                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        if(flag && t>= 0.0 && t<= 1.00){
                            System.out.println("T = " + t);
                            int n = bersierVrh.size() - 1;
                            double ptX = 0;
                            double ptY = 0;
                            double ptZ = 0;
                            for(int i = 0; i <= n; i++){
                                ptX += bersierVrh.get(i).getX() * racunajBersier(n, i, t);
                                ptY += (height-bersierVrh.get(i).getY()) * racunajBersier(n, i, t);
                                ptZ += bersierVrh.get(i).getZ() * racunajBersier(n, i, t);
                            }



                            ociste.setEntry(0,0, ptX);
                            ociste.setEntry(0,1, ptY);
                            ociste.setEntry(0,2, ptZ);
                            /*System.out.println("OCISTE X=" + ociste.getEntry(0, 0) + " Y=" + ociste.getEntry(0, 1) + " Z=" + ociste.getEntry(0, 2) + " H=" + ociste.getEntry(0, 3));
                               System.out.println("GLEDISTE X=" + glediste.getEntry(0, 0) + " Y=" + glediste.getEntry(0, 1) + " Z=" + glediste.getEntry(0, 2) + " H=" + glediste.getEntry(0, 3));
                               System.out.println(); */
                            T = matrixTransformation(ociste, glediste);
                            noviVrh = calculatingTransformations(T, listVrh);
                            noviVrh = calculatingProjections(noviVrh, ociste, glediste);


                            noviVrh = scale(noviVrh, scale, height, width);
                            noviVrh = translate(noviVrh, width, height);

                            T = matrixTransformation(ociste, glediste);


                            noviVrh = calculatingTransformations(T, listVrh);
                            noviVrh = calculatingProjections(noviVrh, ociste, glediste);


                            noviVrh = scale(noviVrh, scale, height, width);
                            noviVrh = translate(noviVrh, width, height);

                            gl2.glBegin(GL.GL_LINES);   //Specifies the primitive or primitives that will be created from vertices presented between glBegin and the subsequent glEnd.
                            draw(gl2, noviVrh, listPoligon);
                            gl2.glEnd();
                        }


                    }


                });

                final JFrame jframe = new JFrame("Transformacija i projekcija");
                jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                jframe.addWindowListener(new WindowAdapter() {

                    public void windowClosing(WindowEvent windowevent) {
                        jframe.dispose();
                        System.exit(0);
                    }
                });

                jframe.getContentPane().add(glcanvas, BorderLayout.CENTER);
                jframe.setSize(640, 480);
                jframe.setVisible(true);
                glcanvas.requestFocusInWindow();
            }

        });
    }


    //MATRICA TRANSFORMACIJE UKUPNA
    private static RealMatrix matrixTransformation (RealMatrix ociste, RealMatrix glediste){
        //PRVA MATRICA TRANSFORMACIJE
        double[][] t1 = {{1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {-ociste.getEntry(0,0), -ociste.getEntry(0,1), -ociste.getEntry(0,2), 1}};
        T1 = new Array2DRowRealMatrix(t1);
        RealMatrix G1 = glediste.multiply(T1);


        //DRUGA MATRICA TRANSFORMACIJE
        double xg1 = G1.getEntry(0,0);
        double yg1 = G1.getEntry(0,1);
        double sinAlpha = yg1 / (Math.sqrt(Math.pow(xg1,2) + Math.pow(yg1,2)));
        double cosAlpha = xg1 / (Math.sqrt(Math.pow(xg1,2) + Math.pow(yg1,2)));

        double[][] t2 = new double[4][4];
        for(int i=0; i<4; i++){
            for(int j=0; j<4; j++){
                if(i == 0 && j==0) t2[i][j] = cosAlpha;
                else if(i == 0 && j==1) t2[i][j] = -sinAlpha;
                else if(i == 1 && j==0) t2[i][j] =  sinAlpha;
                else if(i == 1 && j==1) t2[i][j] = cosAlpha;
                else if(i == j) t2[i][j] = 1;
                else t2[i][j] = 0;
            }
        }
        T2 = new Array2DRowRealMatrix(t2);
        RealMatrix G2 = G1.multiply(T2);


        //TRECA MATRICA TRANSFORMACIJE
        double xg2 = G2.getEntry(0,0);
        double zg2 = G2.getEntry(0, 2);
        double sinBeta = xg2 / (Math.sqrt(Math.pow(xg2,2) + Math.pow(zg2,2)));
        double cosBeta = zg2 / (Math.sqrt(Math.pow(xg2,2) + Math.pow(zg2,2)));
        double[][] t3 = new double[4][4];
        for(int i=0; i<4; i++){
            for(int j=0; j<4; j++){
                if(i == 0 && j==0) t3[i][j] = cosBeta;
                else if(i == 0 && j==2) t3[i][j] = sinBeta;
                else if(i == 2 && j==0) t3[i][j] =  -sinBeta;
                else if(i == 2 && j==2) t3[i][j] = cosBeta;
                else if(i == j) t3[i][j] = 1;
                else t3[i][j] = 0;
            }
        }
        RealMatrix T3 = new Array2DRowRealMatrix(t3);
        RealMatrix G3 = G2.multiply(T3);

        //CETVRTA MATRICA TRANSFORMACIJE
        double[][] t4 = {{0,-1,0,0}, {1,0,0,0}, {0,0,1,0}, {0,0,0,1}};
        RealMatrix T4 = new Array2DRowRealMatrix(t4);

        //PETA MATRICA TRANSFORMACIJE
        double[][] t5 = {{-1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {0,0,0,1}};
        RealMatrix T5 = new Array2DRowRealMatrix(t5);


        RealMatrix Tpom = T2.multiply(T3).multiply(T4).multiply(T5);
        RealMatrix T = T1.multiply(Tpom);
        return T;
    }

    //MNOZENJE SVAKE TOCKE SA ZAVRSNOM MATRICOM TRANSFORMACIJE
    private static List<RealMatrix> calculatingTransformations(RealMatrix T, List<RealMatrix> vrhovi){
        List<RealMatrix> noviVrhovi = new ArrayList<>();
        for(RealMatrix v : vrhovi){
            RealMatrix noviVrh = v.multiply(T);
            /*double[][] d = new double[4][4];
            for(int i=0; i<4; i++){
                for(int j=0; j<4; j++){
                    d[i][j] = Math.abs(noviVrh.getEntry(i,j));
                }
            } */
            //RealMatrix vrh = new Array2DRowRealMatrix(d);
            noviVrhovi.add(noviVrh);
        }
        return noviVrhovi;
    }

    //PROJEKCIJA TOCKE SA MATRICOM PROJEKCIJE
    private static List<RealMatrix> calculatingProjections(List<RealMatrix> transformiraniVrhovi, RealMatrix o, RealMatrix g){
        List<RealMatrix> finalniVrhovi = new ArrayList<>();
        double H = Math.sqrt(Math.pow((o.getEntry(0,0) - g.getEntry(0,0)), 2) + Math.pow((o.getEntry(0,1) - g.getEntry(0,1)), 2) + Math.pow((o.getEntry(0,2) + g.getEntry(0,2)),2));
        double[][] p = {{1,0,0,0}, {0,1,0,0}, {0,0,0,(1/H)}, {0,0,0,0}};
        RealMatrix P = new Array2DRowRealMatrix(p);
        for(RealMatrix v : transformiraniVrhovi){
            RealMatrix Ap = v.multiply(P);

            double[][] tocka = new double[4][4];
            for(int i=0; i<4; i++){
                for(int j=0; j<4; j++){
                    if(i == 0 && j == 0){
                        tocka[i][j] = Ap.getEntry(0,0) / Ap.getEntry(0,3);
                    }
                    else if(i == 0 && j == 1){
                        tocka[i][j] = Ap.getEntry(0,1) / Ap.getEntry(0,3);
                    }
                    else tocka[i][j] = 0;
                }
            }
            RealMatrix finalniVrh = new Array2DRowRealMatrix(tocka);
            finalniVrhovi.add(finalniVrh);
        }
        return finalniVrhovi;
    }

    //CRTAJ
    private static void draw(GL2 gl2, List<RealMatrix> vrhovi, List<iPoligon> poligoni) {
        for(iPoligon poligon : listPoligon){
            gl2.glVertex2f((float)vrhovi.get(poligon.getV1() - 1).getEntry(0,0), (float)vrhovi.get(poligon.getV1() - 1).getEntry(0,1));
            gl2.glVertex2f((float)vrhovi.get(poligon.getV2() - 1).getEntry(0,0), (float)vrhovi.get(poligon.getV2() - 1).getEntry(0,1));
            gl2.glVertex2f((float)vrhovi.get(poligon.getV2() - 1).getEntry(0,0), (float)vrhovi.get(poligon.getV2() - 1).getEntry(0,1));
            gl2.glVertex2f((float)vrhovi.get(poligon.getV3() - 1).getEntry(0,0), (float)vrhovi.get(poligon.getV3() - 1).getEntry(0,1));
            gl2.glVertex2f((float)vrhovi.get(poligon.getV3() - 1).getEntry(0,0), (float)vrhovi.get(poligon.getV3() - 1).getEntry(0,1));
            gl2.glVertex2f((float)vrhovi.get(poligon.getV1() - 1).getEntry(0,0), (float)vrhovi.get(poligon.getV1() - 1).getEntry(0,1));



        }
    }

    //CRTAJ BERSIER
    private static void draw2(GL2 gl2, int x, int y){
        gl2.glVertex2f(x, y);
    }


    //SKALIRAJ RUCNO
    private static List<RealMatrix> scale(List<RealMatrix> listVrh, float scale, int height, int width){
        List<RealMatrix> vrhovi = new ArrayList<>();
        for(RealMatrix vrh : listVrh){
            double[][] v = new double[4][4];
            for(int i=0; i<4; i++){
                for(int j=0; j<4; j++){
                    if(i==0 && j==0) v[i][j] = vrh.getEntry(0,0) * scale;
                    else if(i==0 && j==1) v[i][j] = vrh.getEntry(0,1) * scale ;
                    else if(i==0 && j==2) v[i][j] = vrh.getEntry(0,2) * scale;
                    else if (i==0 && j==3) v[i][j] = 1;
                    else v[i][j] = 0;
                }
            }

            RealMatrix vrh3D = new Array2DRowRealMatrix(v);
            vrhovi.add(vrh3D);
        }
        return vrhovi;
    }

    //TRANSLATIRAJ RUCNO
    private static List<RealMatrix> translate(List<RealMatrix> listVrh, int height, int width){
        List<RealMatrix> translatiraniVrhovi = new ArrayList<>();
        for(RealMatrix vrh : listVrh){
            double[][] v = new double[4][4];
            for(int i=0; i<4; i++){
                for(int j=0; j<4; j++){
                    if(i==0 && j==0) v[i][j] = vrh.getEntry(0,0) + width/2;
                    else if(i==0 && j==1) v[i][j] = vrh.getEntry(0,1) + height/2 ;
                    else if(i==0 && j==2) v[i][j] = vrh.getEntry(0,2) + width/2;
                    else if (i==0 && j==3) v[i][j] = 1;
                    else v[i][j] = 0;
                }
            }

            RealMatrix vrh3D = new Array2DRowRealMatrix(v);
            translatiraniVrhovi.add(vrh3D);
        }

        return translatiraniVrhovi;
    }

    /*private static List<iTocka2D> skalirajBersierVrhove(List<iTocka2D> bersierVrhovi){
        List<iTocka2D> bersier = new ArrayList<>();
        int xPrvaTocka = bersierVrhovi.get(0).getX();
        int yPrvaTocka = bersierVrhovi.get(0).getY();
        for(int i=0; i<bersierVrhovi.size(); i++){
            int xNovi = bersierVrhovi.get(i).getX() - xPrvaTocka;
            int yNovi = bersierVrhovi.get(i).getY() - yPrvaTocka;
            bersier.add(new iTocka2D(xNovi, yNovi));
        }
        return bersier;
    } */

    //EKSTREMI ZA GLEDISTE
    private static void nadiEkstreme(List<RealMatrix> vrhovi) {
        if (vrhovi.size() < 1) return;

        RealMatrix v = vrhovi.get(0);

        double xPom1 = v.getEntry(0,0), yPom1 = v.getEntry(0,1), zPom1 = v.getEntry(0,2);
        double xPom2 = v.getEntry(0,0), yPom2 = v.getEntry(0,1), zPom2 = v.getEntry(0,2);

        for (RealMatrix vrh : vrhovi) {
            if (vrh.getEntry(0,0) > xPom1) xPom1 = vrh.getEntry(0,0);
            if (vrh.getEntry(0,1) > yPom1) yPom1 = vrh.getEntry(0,1);
            if (vrh.getEntry(0,2) > zPom1) zPom1 = vrh.getEntry(0,2);

            if (vrh.getEntry(0,0) < xPom2) xPom2 = vrh.getEntry(0,0);
            if (vrh.getEntry(0,1) < yPom2) yPom2 = vrh.getEntry(0,1);
            if (vrh.getEntry(0,2) < zPom2) zPom2 = vrh.getEntry(0,2);
        }

        x_max = xPom1;
        x_min = xPom2;

        y_max = yPom1;
        y_min = yPom2;

        z_max = zPom1;
        z_min = zPom2;
    }

    //RACUNAJ BERZIERA
    private static double racunajBersier(int n,  int i, double t){
        double razlomak = faktorijel(n) / (faktorijel(i) * faktorijel(n - i));

        return (razlomak * Math.pow(t, i) * Math.pow((1 - t), (n - i)));
    }

    //FAKTORIJEL
    public static int faktorijel(int n) {
        int fact = 1;
        for (int i = 2; i <= n; i++) {
            fact = fact * i;
        }
        return  fact;
    }

}
